package com.geoware.minerva.activity

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.geoware.minerva.*
import com.geoware.minerva.model.ApiModel
import com.geoware.minerva.model.Salud
import com.geoware.minerva.model.Seguridad
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    var previousApiModel: ApiModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fetchFCMToken()

        readDataBase()
        buttonEnterChat.setOnClickListener {
            val launchIntent = packageManager.getLaunchIntentForPackage(ROCKET_CHAT_PACKAGE_NAME)
            if (launchIntent != null) {
                startActivity(launchIntent)
            } else {
                Snackbar.make(
                    mainLayout,
                    getString(R.string.need_rocket_chat_message),
                    Snackbar.LENGTH_LONG
                ).apply {
                    this.setBackgroundTint(
                        ContextCompat.getColor(
                            this@MainActivity,
                            R.color.colorRed
                        )
                    )
                }.show()
            }
        }
    }

    private fun readDataBase() {
        val database = Firebase.database
        val myRef = database.getReference("minerva")

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue<ApiModel>()

                checkForHealthEvents(value?.salud)
                checkForSecurityEvents(value?.seguridad)
                //store previous model to avoid duplicated notifications
                previousApiModel = value
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("TAG", "Failed to read value.", error.toException())
            }
        })
        // [END read_message]
    }


    private fun checkForHealthEvents(health: Salud?) {
        if (health == null) return
        when {
            health.evento_salud?.bradicardia == true && BuildConfig.userType == USER_TYPE_CLIENT -> {
                if (previousApiModel?.salud?.evento_salud?.bradicardia != true) {
                    sendNotification(getString(R.string.bradycardia_alert_message))
                    showAlertMessage(getString(R.string.bradycardia_alert_message))
                }
            }
            health.evento_salud?.bandera_medico == true && BuildConfig.userType == USER_TYPE_SECURITY -> {
                if (previousApiModel?.salud?.evento_salud?.bandera_medico != true) {
                    sendNotification(getString(R.string.cardiac_rythm_alert_message))
                    showAlertMessage(getString(R.string.cardiac_rythm_alert_message))
                }
            }
            health.evento_salud?.bandera_ambulancia == true && BuildConfig.userType == USER_TYPE_SECURITY_911 -> {
                if (previousApiModel?.salud?.evento_salud?.bandera_ambulancia != true) {
                    sendNotification(getString(R.string.incident_alert_911))
                    showAlertMessage(getString(R.string.incident_alert_911))
                }
            }
//            health.armband?.rr_interval == true -> {
//                sendNotification(getString(R.string.cardiac_rythm_alert_message))
//            }
//            health.oximeter?.heart_rate_oxi != null && health.oximeter.heart_rate_oxi > 180 -> {
//                handleHighCriticalRateAlert()
//            }
//            health.oximeter?.heart_rate_oxi != null && health.oximeter.heart_rate_oxi < 60 -> {
//
//            }
        }
    }

    private fun checkForSecurityEvents(security: Seguridad?) {
        if (security == null) return
        when {
            security.evento_seguridad?.intruso == 1 && BuildConfig.userType == USER_TYPE_CLIENT -> {
                if (previousApiModel?.seguridad?.evento_seguridad?.intruso != 1) {
                    sendNotification(getString(R.string.housebreaker_alert_message))
                    showAlertMessage(getString(R.string.housebreaker_alert_message))
                }
            }
            security.evento_seguridad?.bandera_caseta == true && BuildConfig.userType == USER_TYPE_SECURITY -> {
                if (previousApiModel?.seguridad?.evento_seguridad?.bandera_caseta != true) {
                    sendNotification(getString(R.string.housebreaker_alert_message))
                    showAlertMessage(getString(R.string.housebreaker_alert_message))
                }
            }
            security.evento_seguridad?.bandera_911 == true && BuildConfig.userType == USER_TYPE_SECURITY_911 -> {
                // this code prevent to duplicate notifications if another field is changed
                if (previousApiModel?.seguridad?.evento_seguridad?.bandera_911 != true) {
                    //create a notification with a message
                    sendNotification(getString(R.string.incident_alert_911))
                    //show message in case the user does not want to access to the chat though the notification
                    showAlertMessage(getString(R.string.incident_alert_911))
                }
            }
        }
    }

    private fun showAlertMessage(message: String) {
        buttonEnterChat.visibility = View.VISIBLE
        alertMessage.text = message
    }

    private fun sendNotification(message: String) {
        val launchIntent = packageManager.getLaunchIntentForPackage(ROCKET_CHAT_PACKAGE_NAME)
        launchIntent?.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        var pendingIntent: PendingIntent? = null

        launchIntent?.let {
            pendingIntent = PendingIntent.getActivity(
                this, 0, launchIntent,
                PendingIntent.FLAG_ONE_SHOT
            )
        }

        var notificationBuilder: NotificationCompat.Builder? = null
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val alarmSound =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val mp = MediaPlayer.create(applicationContext, alarmSound)
        mp.start()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                packageName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = packageName
            channel.enableVibration(true)
            notificationManager.createNotificationChannel(channel)
            if (notificationBuilder == null) {
                notificationBuilder = NotificationCompat.Builder(application, CHANNEL_ID)
            }
        } else {
            if (notificationBuilder == null) {
                notificationBuilder = NotificationCompat.Builder(application, CHANNEL_ID)
            }
        }
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(getString(R.string.app_name))
            .setContentText(message)
            .setAutoCancel(true)
            .setSound(alarmSound)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .setCategory(NotificationCompat.CATEGORY_ALARM)

        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun fetchFCMToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.d("FcmMessageService", "failed token: ${task.exception}")
                    return@OnCompleteListener
                }
                //TODO: once we have a database that stores the token, store it.
                val token = task.result?.token
                Log.d("FcmMessageService", "token: ${task.result?.token}")
            })
    }
}
