package com.geoware.minerva.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.geoware.minerva.R
import com.geoware.minerva.ROCKET_CHAT_PACKAGE_NAME

class RocketChatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rocket_chat)

        val launchIntent = packageManager.getLaunchIntentForPackage(ROCKET_CHAT_PACKAGE_NAME)
        launchIntent?.let {
            startActivity(it)
        }
    }
}
