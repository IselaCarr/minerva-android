package com.geoware.minerva.model

data class ApiModel(
    val salud: Salud? = null,
    val seguridad: Seguridad? = null
)

data class Salud(
    val armband: Armband? = null,
    val evento_salud: EventoSalud? = null,
    val oximeter: Oximeter? = null
)

data class Armband(
    val heart_rate_arm: Int? = null,
    val rr_interval: Boolean? = null
)

data class EventoSalud(
    val bandera_ambulancia: Boolean? = null,
    val bandera_medico: Boolean? = null,
    val bradicardia: Boolean? = null
)

data class Oximeter(
    val heart_rate_oxi: Int? = null,
    val perfusion_index: Double? = null,
    val saturation: Int? = null
)

data class Seguridad(
    val evento_seguridad: EventoSeguridad? = null,
    val multimedia: Multimedia? = null
)

data class EventoSeguridad(
    val bandera_911: Boolean? = null,
    val bandera_caseta: Boolean? = null,
    val intruso: Int? = null
)

data class Multimedia(
    val imagen: String? = null,
    val video: String? = null
)