package com.geoware.minerva

const val USER_TYPE_CLIENT = "Client"
const val USER_TYPE_SECURITY = "Security"
const val USER_TYPE_SECURITY_911 = "Security911"

const val ROCKET_CHAT_PACKAGE_NAME = "chat.rocket.android"